<?php
namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\Model;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('oldPassword', PasswordType::class, [
            'label' => 'Stare hasło',
            //'mapped' => false,
            'required' => true,
            'constraints' => [
                new NotBlank([
                    'message' => 'Wprowadź hasło.',
                ])
            ],
        ])

        ->add('newPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'mapped' => false,
            'required' => true,
            'constraints' => [
                new NotBlank([
                    'message' => 'Wprowadź hasło.',
                ]),
                new Length([
                    'min' => 6,
                    'minMessage' => 'Twoje hasło powinno mieć conajmniej {{ limit }} znaków.',
                    // max length allowed by Symfony for security reasons
                    'max' => 4096,
                ]),
            ],
            'first_options' => ['label' => 'Hasło'],
            'second_options' => ['label' => 'Powtórz hasło'],
            'invalid_message' => 'Hasła się nie zgadzają.'
        ]);
        /*
        $builder->add('oldPassword', 'password');
        $builder->add('newPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'required' => true,
            'first_options'  => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat Password'),
        ));
        */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Model\ChangePassword',
        ]);
    }
}
