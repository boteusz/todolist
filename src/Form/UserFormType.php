<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                //'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Wprowadź e-mail.',
                    ]),
                    new Email([
                        'message' => 'Wprowadź poprawny adres e-mail.'
                        ]),
                    new Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('birthDate', BirthdayType::class, [
                'label' => 'Data urodzenia',
                'format' => 'dd-MM-yyyy',
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'Płeć',
                'choices'  => [
                    'Inna/Brak' => null,
                    'Mężczyzna' => true,
                    'Kobieta' => false,
                ],
            ])
            ->add('avatarFilename', FileType::class, [
                'mapped' => false,
                'label' => 'Avatar (plik graficzny)',
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/bmp',
                            'image/png',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Proszę użyj poprawnego pliku bmp lub png.',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
