<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

//use Symfony\Component\Intl\Languages;


class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, string $safeFilename)
    {
        //$originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $newFileName = $safeFilename.'.'.$file->guessExtension();

        try {
            $file->move(
                $this->getTargetDirectory(),
                $newFileName
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $newFileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
