<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserFormType;
use App\Form\ChangePasswordFormType;
use App\Form\Model\ChangePassword;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\FileUploader;
use Psr\Log\LoggerInterface;

class AccountController extends AbstractController
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/account", name="modify_account")
     */
    public function account(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        FileUploader $fileUploader
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $formData = $this->createForm(UserFormType::class);
        $formData->get('email')->setData($user->getEmail());
        $formData->get('birthDate')->setData($user->getBirthDate());
        $formData->get('gender')->setData($user->getGender());
        $formData->handleRequest($request);
        $avatarExists = $user->getAvatarFilename() != null;

        $changeData = new ChangePassword();
        $formPassChange = $this->createForm(ChangePasswordFormType::class, $changeData);
        $formPassChange->handleRequest($request);

        if ($formPassChange->isSubmitted() && $formPassChange->isValid()) {
            $newPwd = $formPassChange->get('newPassword')->getData();

            $this->logger->info("Zmieniam hasło użytkownika!");
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $newPwd
                )
            );
            $entityManager->flush();

            $this->addFlash(
                'Komunikat',
                'Pomyślnie zmieniono hasło!'
            );
            //return $this->redirectToRoute('index');
        }
        
        if ($formData->isSubmitted() && $formData->isValid()) {
            $user->setEmail($formData->get('email')->getData());
            $user->setBirthDate($formData->get('birthDate')->getData());

            $user->setGender($formData->get('gender')->getData());
            
            /** @var UploadedFile $avatarFile */
            $avatarFile = $formData['avatarFilename']->getData();

            if ($avatarFile) {
                $avatarFilename = $fileUploader->upload($avatarFile, $user->getUsername().'_avatar');
                $user->setAvatarFilename($avatarFilename);
            }

            $entityManager->flush();

            $this->addFlash(
                'Komunikat',
                'Zaktualizowano dane użytkownika!'
            );

            //return $this->redirectToRoute('index');
        }

        return $this->render('account/modify.html.twig', [
            'accountForm' => $formData->createView(),
            'passwordChangeForm' => $formPassChange->createView(),
            'avatarExists' => $avatarExists,
        ]);
    }
}
