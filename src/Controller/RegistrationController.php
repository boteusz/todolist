<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use App\Security\LoginFormAuthenticator;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use SwiftMailer;

class RegistrationController extends AbstractController
{
    protected $mailer;
    protected $logger;

    public function __construct(\Swift_Mailer $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $mail = $form->get('email')->getData();
            $user->setEmail($mail);
            $user->setApiToken(bin2hex(random_bytes(14)));

            $user->setConfirmationToken(bin2hex(random_bytes(10)));

            $user->setEnabled(false);

            // do anything else you need here, like send an email
            $this->sendConfirmationEmailMessage($user);

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_registration_mail_sent');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    public function sendConfirmationEmailMessage(User $user)
    {
        $confirmationToken = $user->getConfirmationToken();
        $username = $user->getUsername();

        $subject = 'Aktywacja konta';
        $email = $user->getEmail();

        $renderedTemplate = $this
        //->templating
        ->renderView('emails/registration.html.twig', [
            'username' => $username,
            'confirmationToken' => $confirmationToken
        ]);

        $message = (new \Swift_Message("Registration Mail"))
            ->setSubject($subject)
            ->setFrom('rejestracja@localhost')
            //->setReplyTo(MAILER_URL)
            ->setTo($email)
            ->setBody($renderedTemplate, "text/html");
        try {
            $this->mailer->send($message);
        } catch (\Swift_TransportException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
    * @Route("user/activate/{token}", name="account_confirmation")
    */
    public function confirmAction(
        Request $request,
        $token,
        UserRepository $repository,
        EntityManagerInterface $entityManager
    ) {
        //$em = $this->getDoctrine()->getManager();
        //$repository = $em->getRepository('AppBundle:User');

        $user = $repository->findUserByConfirmationToken($token);

        if (!$user) {
            //throw $this->createNotFoundException('We couldn\'t find an account for that confirmation token');
            return $this->redirectToRoute('index');
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('user_registration_confirmed');
    }

    /**
     * @Route("/register/activated", name="user_registration_confirmed")
     */
    public function userActivated()
    {
        return $this->render('registration/confirmed.html.twig');
    }

    /**
     * @Route("/register/pending", name="user_registration_mail_sent")
     */
    public function userPromptToActivate()
    {
        return $this->render('registration/registerMail.html.twig');
    }
}
