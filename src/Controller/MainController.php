<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TaskListRepository;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function mainIndex()
    {
        return $this->render('main/main.html.twig');
    }
}
