<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\TaskList;
use App\Repository\TaskListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use App\Form\TaskListEditFormType;

class TaskListController extends AbstractController
{
    /**
     * @Route("/lists/{username}", name="user_task_lists");
     */
    public function index($username, LoggerInterface $logger)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        
        if ($username != $user->getUsername()) {
            return $this->redirectHere($user);
        }
        
        $taskLists = $user->getLists();

        return $this->render('task_list/index.html.twig', [
            'taskLists' => $taskLists,
            'user' => $user,
        ]);
    }

    private function redirectHere($user)
    {
        return $this->redirectToRoute('user_task_lists', array('username' => $user->getUsername()));
    }

    /**
     * @Route("/lists/{username}/list/{id}/remove", name="remove_list")
     */
    public function removeList(
        $username,
        $id,
        EntityManagerInterface $entityManager,
        TaskListRepository $taskListRepository
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        if ($username != $user->getUsername()) {
            return $this->redirectHere($user);
        }
        
        $taskList = $taskListRepository->find($id);
        if ($taskList) {
            $entityManager->remove($taskList);
            $entityManager->flush();
        }

        return $this->redirectHere($user);
    }

    /**
     * @Route("/lists/{username}/list/{id}/edit", name="edit_list_name")
     */
    public function editListName(
        $username,
        $id,
        Request $request,
        EntityManagerInterface $entityManager,
        TaskListRepository $taskListRepository
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        if ($username != $user->getUsername()) {
            return $this->redirectHere($user);
        }
        
        $taskList = $taskListRepository->find($id);
        if (!$taskList) {
            //throw $this->createNotFoundException('Nie znaleziono takiej listy!');
            return $this->redirectHere($user);
        }

        $newTaskList = new TaskList();

        $form = $this->createForm(TaskListEditFormType::class, $newTaskList);
        $form->get('name')->setData($taskList->getName());

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $newName = $form->get('name')->getData();
            $taskList->setName($newName);

            $entityManager->flush();

            return $this->redirectToRoute('user_view_list', array('username' => $user->getUsername(),'id' => $id));
        }
        //$entityManager->flush();

        return $this->render('task_list/edit.html.twig', [
            'taskList' => $taskList,
            'editTaskListForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/lists/{username}/new", name="create_empty_list")
     */
    public function createEmptyList($username, EntityManagerInterface $entityManager)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $newTaskList = new TaskList();
        $newTaskList->setName('Nowa lista');
        $newTaskList->setOwner($user);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($newTaskList);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        
        return $this->redirectHere($user);
    }
}
