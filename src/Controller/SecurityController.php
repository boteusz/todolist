<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use SwiftMailer;
use Psr\Log\LoggerInterface;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use App\Form\SetPasswordFormType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    protected $mailer;
    protected $logger;

    public function __construct(\Swift_Mailer $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @Route("/login/remind/{username}", name="remind_password")
     */
    public function sendResetPasswordMail($username, UserRepository $userRepo, EntityManagerInterface $entityManager)
    {
        $user = $userRepo->findUserByUsername($username);

        if ($user) {
            if (!$user->getEnabled()) {
                $this->logger->error("Użytkownik nie był aktywowany...");
                return $this->redirectToRoute('index');
            }

            $resetToken = bin2hex(random_bytes(12));
            
            $subject = 'Resetowanie hasła';
            $email = $user->getEmail();

            $renderedTemplate = $this->renderView('emails/resetPassword.html.twig', [
            'username' => $username,
            'resetToken' => $resetToken
            ]);

            $message = (new \Swift_Message("Reset Password"))
                ->setSubject($subject)
                ->setFrom('odzyskiwanie@localhost')
                ->setTo($email)
                ->setBody($renderedTemplate, "text/html");
        
            try {
                $this->mailer->send($message);
            } catch (\Swift_TransportException $e) {
                $this->logger->error($e->getMessage());
            }

            $this->logger->info("e-mail has been sent");
            
            $user->setPasswordResetToken($resetToken);

            $entityManager->flush();

            return $this->redirectToRoute('account_reset_mail_sent');
        } else {
            $this->logger->error("There was no such user");
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/login/reset/{username}/{token}", name="account_reset")
     */
    public function resetPassword(
        $username,
        $token,
        UserRepository $userRepo,
        EntityManagerInterface $entityManager,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $user = $userRepo->findUserByUsername($username);

        if (!$user) {
            $this->logger->error("There was no such user!");
            return $this->redirectToRoute('index');
        }

        if ($user->getPasswordResetToken() != $token) {
            $this->logger->error("Reset password token doesn't match!");
            return $this->redirectToRoute('index');
        }

        $form = $this->createForm(SetPasswordFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setPasswordResetToken(null);

            $entityManager->flush();

            return $this->redirectToRoute('account_reset_success');
        }
        
        return $this->render('security/passwordReset.html.twig', [
            'username' => $username,
            'resetPasswordForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login/resetsuccess", name="account_reset_success")
     */
    public function resetPasswordSuccess()
    {
        return $this->render('security/passwordResetSuccess.html.twig');
    }

    /**
     * @Route("/login/resetmailsent", name="account_reset_mail_sent")
     */
    public function resetPasswordMailSentSuccess()
    {
        return $this->render('security/passwordResetMail.html.twig');
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
