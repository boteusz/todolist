<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Task;
use App\Entity\TaskList;
use App\Repository\TaskRepository;
use App\Repository\TaskListRepository;
use App\Form\TaskFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Package;
use Symfony\Component\HttpFoundation\JsonResponse;

class TaskController extends AbstractController
{
    /**
     * @Route("/lists/{username}/list/{id}", name="user_view_list")
     */
    public function index(
        $username,
        $id,
        TaskListRepository $taskListRepository,
        EntityManagerInterface $entityManager,
        Request $request,
        LoggerInterface $logger
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        
        if ($username != $user->getUsername()) {
            return $this->redirectToRoute('user_task_lists', array('username' => $user->getUsername()));
        }

        $taskList = $taskListRepository->find($id);

        if (!$taskList) {
            //throw $this->createNotFoundException('Nie znaleziono takiej listy!');
            return $this->redirectToRoute('user_task_lists', array('username' => $user->getUsername()));
        }
        
        $newTask = new Task();

        $form = $this->createForm(TaskFormType::class, $newTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newTask->setParentList($taskList);
            $newTask->setCompleted(false);
            $newTask->setOrderId(0);
            $entityManager->persist($newTask);
            $entityManager->flush();

            return $this->redirectHere($user, $id);
        }


        return $this->render('task/index.html.twig', [
            'newTaskForm' => $form->createView(),
            'taskList' => $taskList,
        ]);
    }

    /**
     * @Route("/lists/{username}/list/{id}/checkbox", name="task_change_checkbox")
     */
    public function checkboxAction(
        Request $request = null,
        LoggerInterface $logger,
        TaskRepository $taskRepository,
        EntityManagerInterface $entityManager
    ) {
        if ($request) {
            $taskId = $request->request->get('taskId'); //id
            $taskValue = $request->request->get('taskValue'); // bool
            $logger->info('CheckboxAction works');
            $changedTask = $taskRepository->find($taskId);
            if ($changedTask) {
                $logger->info('Task found!');
                $logger->info($taskValue);
                $finalBoolean = $taskValue === 'true'; // konwersja ze string na boolean
                $changedTask->setCompleted($finalBoolean);
                $entityManager->flush();
            }
        } else {
            $logger->info('There was no request :(');
        }
        $response = array('code' => 100, 'success' => true);

        return new JsonResponse($response);
    }

    /**
     * @Route("/lists/{username}/list/{id}/task/{taskId}", name="edit_task")
     */
    public function editTask(
        $username,
        $id,
        $taskId,
        TaskRepository $taskRepository,
        EntityManagerInterface $entityManager,
        Request $request,
        LoggerInterface $logger
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        
        if ($username != $user->getUsername()) {
            return $this->redirectToRoute('user_task_lists', array('username' => $user->getUsername()));
        }

        $task = $taskRepository->find($taskId);

        if (!$task) {
            return $this->redirectHere($user, $id);
        }
        
        $newTask = new Task();

        $form = $this->createForm(TaskFormType::class, $newTask);
        $form->get('content')->setData($task->getContent());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newContent = $form->get('content')->getData();

            $task->setContent($newContent);

            $entityManager->flush();

            return $this->redirectHere($user, $id);
        }


        return $this->render('task/edit.html.twig', [
            'editTaskForm' => $form->createView(),
            'listId' => $id,
            'taskId' => $taskId,
        ]);
    }

    /**
     * @Route("/lists/{username}/list/{id}/task/{taskId}/remove", name="remove_task")
     */
    public function removeTask(
        $username,
        $id,
        $taskId,
        TaskRepository $taskRepository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        
        if ($username != $user->getUsername()) {
            return $this->redirectToRoute('user_task_lists', array('username' => $user->getUsername()));
        }

        $task = $taskRepository->find($taskId);

        if ($task) {
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectHere($user, $id);
    }


    private function redirectHere($user, $id)
    {
        return $this->redirectToRoute('user_view_list', array('username' => $user->getUsername(),'id' => $id));
    }
}
